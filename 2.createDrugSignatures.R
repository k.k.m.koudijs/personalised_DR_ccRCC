library("biomaRt")
library("limma")
library("cmapR")
library("readr")

source("Functions/getDrugAndControlInstances.R")
source("Functions/loadLINCSmeta.R")
source("Functions/getDEGfromLimma.R")
source("Functions/createDesignMatrix.R")
loadLINCSmeta()


tumorCells <- subset(cell_info, sample_type=="tumor")$cell_id
drugs <- # 19,812 drugs => good for 672,128 instances (51% of total, excluding control samples)
  unique(subset(inst_info, pert_type=="trt_cp")$pert_iname)

gctx_path <- # Very big file, downloadable from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE92742
  "LINCSdata/GSE92742_Broad_LINCS_Level3_INF_mlr12k_n1319138x12328.gctx" 

ensembl <-
  useMart("ensembl", dataset = "hsapiens_gene_ensembl")
mappingTable <- 
  getBM(attributes = c("entrezgene","ensembl_gene_id", "hgnc_symbol"), mart = ensembl)
source("Functions/getEntrezToEnsembleMappingTable.R")
mappingTable_LM <-
  getEntrezToEnsembleMappingTable(mappingTable, subset(gene_info, pr_is_lm==1)$pr_gene_id) # 937 landmark (LM) genes map to unique Ensembl
mappingTable_LM$entrezgene <- as.character(mappingTable_LM$entrezgene)


DEG_LINCSdrugs <-
  data.frame(
    estimate=character(),
    tstat=numeric(),
    pvalue=numeric(),
    fdr=numeric(),
    gene=character(),
    drug=character()
  )

for (drug in drugs){
  
  print(paste0("Drug = ",drug))
  
  drug_and_control_instances <-
    getDrugAndControlInstances(drugs = drug, instances = inst_info, cells = tumorCells)
  
  data_subset <-
    parse.gctx(
      fname = gctx_path, 
      rid = mappingTable_LM$entrezgene,
      cid = drug_and_control_instances$id
    )@mat
  
  # sum(rownames(data_subset) == mappingTable_LM$entrezgene) == nrow(data_subset) # Order is preserved. Therefore:
  rownames(data_subset) <- mappingTable_LM$ensembl_gene_id

  DEG_limma <-
    getDEGfromLimma(
      data=data_subset, 
      instances=drug_and_control_instances
    ) # Sometimes gives the warning "Partial NA coefficients for 937 probe(s)" and "Estimation of var.prior failed - set to default value"
  # but the output is otherwise identical as with lm function (just a lot faster)
   
  rownames(DEG_limma) <- NULL
  DEG_limma$drug <- drug 

  DEG_LINCSdrugs <-
    rbind(DEG_LINCSdrugs, DEG_limma)
  
}

# Some drugs failed to generate results, 19424 left.
DEG_LINCSdrugs <- DEG_LINCSdrugs[order(DEG_LINCSdrugs$drug, DEG_LINCSdrugs$gene),] # For uniformity's sake
drugSignatures <- DEG_LINCSdrugs
save(drugSignatures,file="ProcessedData/drugSignatures.Rdata")