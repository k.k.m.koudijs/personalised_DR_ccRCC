load("TumourPuritySensitivityAnalysis/Input/complete_full_mut_chr_summary.RData")
load("TumourPuritySensitivityAnalysis/Input/tumorTissueMetaData.RData")
load("TumourPuritySensitivityAnalysis/Input/valid_kirc_samples.RData")
load("HypothesisGeneration/KIRC_processed_RNAseq.RData")

library(MASS)
library(ggplot2)
library(reshape2)
library(readxl)
tumor_purity <- 
  subset(
    read_excel(path="TumourPuritySensitivityAnalysis/Input/ncomms9971-s2.xlsx", skip=2), 
    `Cancer type`=="KIRC", 
    select = c("Sample ID","ESTIMATE","ABSOLUTE","LUMP","IHC","CPE"))

# Removes samples which failed Toil quality check:
complete_full_mut_chr <- complete_full_mut_chr[complete_full_mut_chr$signature_id %in% valid_kirc_samples,] # lost 1

# Remove samples no 3p loss:
#complete_full_mut_chr <- subset(complete_full_mut_chr, Loss_3p21.1==TRUE, select=-Loss_3p21.1)

tumor_purity$LUMP <- 100 * as.numeric(tumor_purity$LUMP) # Methylation based 
tumor_purity$ABSOLUTE <- 100 * as.numeric(tumor_purity$ABSOLUTE) # DNA based
tumor_purity$ESTIMATE <- 100 * as.numeric(tumor_purity$ESTIMATE) # RNA based
tumor_purity$IHC <- 100 * as.numeric(tumor_purity$IHC) # Immunohistochemistry
tumor_purity$CPE <- 100 * as.numeric(tumor_purity$CPE) # Combined
summary(tumor_purity)

# Removes samples  >80% CPE:
complete_full_mut_chr <- merge(x=complete_full_mut_chr, y=tumor_purity, by.x="sample_id", by.y="Sample ID")
complete_full_mut_chr <- subset(complete_full_mut_chr, CPE < 80)

# 272 samples left
summary(complete_full_mut_chr[,3:35])
remove_columns <- c("PIK3CA","PTEN","MYC","TSC1","TSC2","TCEB1")
complete_full_mut_chr <- complete_full_mut_chr[,!colnames(complete_full_mut_chr) %in% remove_columns]
complete_full_mut_chr <- complete_full_mut_chr[order(complete_full_mut_chr$signature_id),]


tumorData <- tumorData[,colnames(tumorData) %in% complete_full_mut_chr$signature_id]
tumorData <- t(tumorData)
tumorData <- tumorData[order(rownames(tumorData)),]
normalData <- t(normalData)
normalData <- normalData[order(rownames(normalData)),]

complete_full_mut_chr$signature_id == rownames(tumorData) # same order

covariates <- c("TP53","VHL","PBRM1","BAP1",
                "SETD2","KDM5C","MTOR","ARID1A","CSMD3","Gain_5q35",              
                "Loss_14q24","Loss_9p21.3","Loss_6q26","Loss_8p11","Loss_10q23","Loss_1p36",                
                "Loss_4q35","Gain_8q24","Gain_3q26","Loss_13q21","Gain_1q32","Loss_15q21",               
                "Loss_2q37","Gain_2q","Gain_7q","Gain_12p","Gain_20q")


glm_output_null_model <- data.frame(matrix(
  nrow = ncol(tumorData), ncol = 5
))
colnames(glm_output_null_model) <- c("Gene","Intercept","Type","Type_pvalue","LRT_pvalue")

glm_output_3p_model <- data.frame(matrix(
  nrow = ncol(tumorData), ncol = 5
))
colnames(glm_output_3p_model) <- c("Gene","Intercept","Loss_3p21.1","Loss_3p21.1_pvalue","LRT_pvalue")



glm_output_full_model <- data.frame(matrix(
  nrow = ncol(tumorData), ncol = 59
))
colnames(glm_output_full_model) <- 
  c("Gene","Intercept","LRT_pvalue","Type",covariates, paste0(c("Type",covariates),"_pvalue"))


glm_output_full_model_incl_3p <- data.frame(matrix(
  nrow = ncol(tumorData), ncol = 61
))
colnames(glm_output_full_model_incl_3p) <- 
  c("Gene","Intercept","LRT_pvalue","Type","Loss_3p21.1",covariates, paste0(c("Type","Loss_3p21.1",covariates),"_pvalue"))



glm_output_purity_model <- data.frame(matrix(
  nrow = ncol(tumorData), ncol = 9
))
colnames(glm_output_purity_model) <- c(
  "Gene",
  "Intercept_CPE","CPE","CPE_pvalue","LRT_CPE_pvalue",
  "Intercept_IHC","IHC","IHC_pvalue","LRT_IHC_pvalue"
)


glm_input <- complete_full_mut_chr
glm_input$Type <- "Tumor"
normal_indices <- (nrow(glm_input)+1):(nrow(glm_input)+nrow(normalData))
glm_input[normal_indices,"signature_id"] <- rownames(normalData)
glm_input[normal_indices,"Type"] <- "Normal"
glm_input[normal_indices,"Loss_3p21.1"] <- FALSE
glm_input[normal_indices,covariates] <- FALSE

 
save(glm_input, file="HypothesisGeneration/glm_input.RData")


for (i in 1:ncol(tumorData)){
  
  print(i)
  
  glm_input$expression_count <- NA
  glm_input$expression_count[1:nrow(tumorData)] <- as.integer(tumorData[,i])
  glm_input$expression_count[normal_indices] <- as.integer(normalData[,i])
  
  if (FALSE){
    
    a <- tryCatch(
      {
        
        output <- list()
        
        null_null_model <- 
          glm.nb(
            formula="expression_count ~ 1", 
            data=subset(glm_input, Loss_3p21.1==TRUE | Type=="Normal"), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        null_model <- 
          glm.nb(
            formula="expression_count ~ Type", 
            data=subset(glm_input, Loss_3p21.1==TRUE | Type=="Normal"), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        output$coef <- coef(summary(null_model))
        
        if (nrow(output$coef) > 1){
          
          glm_output_null_model[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_null_model[i,"Intercept"] <-  output$coef[1,"Estimate"]
          glm_output_null_model[i,"Type"] <-  output$coef[2,"Estimate"]
          glm_output_null_model[i,"Type_pvalue"] <-  output$coef[2,"Pr(>|z|)"]        
          glm_output_null_model[i,"LRT_pvalue"] <- anova(null_null_model, null_model)[[8]][2]
          
        }
      },
      error=function(cond) {
        return(NA)
      },
      warning=function(cond) {
        return(NULL)
      }
    )    

  }

  if (FALSE){
    
    b <- tryCatch(
      {
        
        output <- list()
        
        null_model <- 
          glm.nb(
            formula="expression_count ~ 1", 
            data=subset(glm_input, Type=="Tumor"), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        chr_3p_model <- 
          glm.nb(
            formula="expression_count ~ Loss_3p21.1", 
            data=subset(glm_input, Type=="Tumor"), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        
        output$coef <- coef(summary(chr_3p_model))
        
        if (nrow(output$coef) > 1){
          
          glm_output_3p_model[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_3p_model[i,"Intercept"] <-  output$coef[1,"Estimate"]
          glm_output_3p_model[i,"Loss_3p21.1"] <-  output$coef[2,"Estimate"]
          glm_output_3p_model[i,"Loss_3p21.1_pvalue"] <-  output$coef[2,"Pr(>|z|)"]
          glm_output_3p_model[i,"LRT_pvalue"] <- anova(null_model, chr_3p_model)[[8]][2]
          
        }
        
      },
      error=function(cond) {
        return(NA)
      },
      warning=function(cond) {
        return(NULL)
      }
    )  
  }
  
  if (FALSE){
    
    c <- tryCatch(
      {
        
        output <- list()
        
        null_model <- 
          glm.nb(
            formula="expression_count ~ Type", 
            data=subset(glm_input, Loss_3p21.1==TRUE | Type=="Normal"), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        full_model <- 
          glm.nb(
            formula=paste0("expression_count ~ Type + ",paste(covariates, collapse = " + ")), 
            data=subset(glm_input, Loss_3p21.1==TRUE | Type=="Normal"),
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        output$coef <- coef(summary(full_model))
        output$LRT_pvalue <- anova(null_model, full_model)[[8]][2]
        
        if (!is.na(output$LRT_pvalue)){
          
          glm_output_full_model[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_full_model[i,"Intercept"] <-  output$coef[1,"Estimate"]
          glm_output_full_model[i,"LRT_pvalue"] <-output$LRT_pvalue
          
          glm_output_full_model[i,c("Type",covariates)] <- as.numeric(output$coef[2:nrow(output$coef),1])
          glm_output_full_model[i,paste0(c("Type",covariates),"_pvalue")] <- as.numeric(output$coef[2:nrow(output$coef),4])
          
        }
        
      },
      error=function(cond) {
        return(NA)
      },
      warning=function(cond) {
        return(NULL)
      }
    )
    
  }
  
  if (FALSE){
    
    d <- tryCatch(
      {
        
        output <- list()
        
        null_model <- 
          glm.nb(
            formula="expression_count ~ 1", 
            data=subset(glm_input, Type=="Tumor" & Loss_3p21.1==TRUE), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        CPE_model <- 
          glm.nb(
            formula="expression_count ~ CPE", 
            data=subset(glm_input, Type=="Tumor" & Loss_3p21.1==TRUE), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        IHC_model <- 
          glm.nb(
            formula="expression_count ~ IHC", 
            data=subset(glm_input, Type=="Tumor" & Loss_3p21.1==TRUE), 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        
        output$coef_CPE <- coef(summary(CPE_model))
        output$coef_IHC <- coef(summary(IHC_model))
        
        if (nrow(output$coef_CPE) > 1){
          
          glm_output_purity_model[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_purity_model[i,"Intercept_CPE"] <-  output$coef_CPE[1,"Estimate"]
          glm_output_purity_model[i,"CPE"] <-  output$coef_CPE[2,"Estimate"]
          glm_output_purity_model[i,"CPE_pvalue"] <-  output$coef_CPE[2,"Pr(>|z|)"]
          glm_output_purity_model[i,"LRT_CPE_pvalue"] <- anova(null_model, CPE_model)[[8]][2]
          
        }
        
        if (nrow(output$coef_IHC) > 1){
          
          glm_output_purity_model[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_purity_model[i,"Intercept_IHC"] <-  output$coef_IHC[1,"Estimate"]
          glm_output_purity_model[i,"IHC"] <-  output$coef_IHC[2,"Estimate"]
          glm_output_purity_model[i,"IHC_pvalue"] <-  output$coef_IHC[2,"Pr(>|z|)"]
          glm_output_purity_model[i,"LRT_IHC_pvalue"] <- anova(null_model, IHC_model)[[8]][2]
          
        }

      },
      error=function(cond) {
        return(NA)
      },
      warning=function(cond) {
        return(NULL)
      }
    )    
  }
  
  if (TRUE){
    
    e <- tryCatch(
      {
        
        output <- list()
        
        null_model <- 
          glm.nb(
            formula="expression_count ~ Type", 
            data=glm_input, 
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        full_model_incl_3p <- 
          glm.nb(
            formula=paste0("expression_count ~ Type + Loss_3p21.1 + ",paste(covariates, collapse = " + ")), 
            data=glm_input,
            control= glm.control(epsilon = 1e-8, maxit = 10000, trace = FALSE)
          )
        
        output$coef <- coef(summary(full_model_incl_3p))
        output$LRT_pvalue <- anova(null_model, full_model_incl_3p)[[8]][2]
        
        if (!is.na(output$LRT_pvalue)){
          
          glm_output_full_model_incl_3p[i,"Gene"] <- colnames(tumorData)[i]
          glm_output_full_model_incl_3p[i,"Intercept"] <-  output$coef[1,"Estimate"]
          glm_output_full_model_incl_3p[i,"LRT_pvalue"] <-output$LRT_pvalue
          
          glm_output_full_model_incl_3p[i,c("Type","Loss_3p21.1",covariates)] <- as.numeric(output$coef[2:nrow(output$coef),1])
          glm_output_full_model_incl_3p[i,paste0(c("Type","Loss_3p21.1",covariates),"_pvalue")] <- as.numeric(output$coef[2:nrow(output$coef),4])
          
        }
        
      },
      error=function(cond) {
        return(NA)
      },
      warning=function(cond) {
        return(NULL)
      }
    )
    
  }
  
  
}

glm_output_null_model$LRT_fdr <- p.adjust(glm_output_null_model$LRT_pvalue, method="BH")
glm_output_3p_model$LRT_fdr <- p.adjust(glm_output_3p_model$LRT_pvalue, method="BH")
glm_output_full_model$LRT_fdr <- p.adjust(glm_output_full_model$LRT_pvalue, method="BH")
glm_output_purity_model$LRT_CPE_fdr <- p.adjust(glm_output_purity_model$LRT_CPE_pvalue, method="BH")
glm_output_purity_model$LRT_IHC_fdr <- p.adjust(glm_output_purity_model$LRT_IHC_pvalue, method="BH")
glm_output_full_model_incl_3p$LRT_fdr <- p.adjust(glm_output_full_model_incl_3p$LRT_pvalue, method="BH")

#save(
#  glm_output_null_model, glm_output_3p_model,glm_output_full_model, glm_output_purity_model,glm_output_full_model_incl_3p,
#  file="HypothesisGeneration/glm_output.RData"
#)

sum(glm_output_null_model$LRT_fdr < 0.01, na.rm=TRUE)
sum(glm_output_3p_model$LRT_fdr < 0.01, na.rm=TRUE)
sum(glm_output_full_model$LRT_fdr < 0.01, na.rm=TRUE)
sum(glm_output_purity_model$LRT_CPE_fdr < 0.01, na.rm=TRUE)
sum(glm_output_purity_model$LRT_IHC_fdr < 0.01, na.rm=TRUE)