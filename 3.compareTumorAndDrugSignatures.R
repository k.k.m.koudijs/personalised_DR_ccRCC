library("PharmacoGx")
options(stringsAsFactors = FALSE)

load(file="ProcessedData/tumorSignatures.RData")
load(file="ProcessedData/drugSignatures.Rdata")

sharedGenes <- intersect(unique(tumorSignatures$gene), unique(drugSignatures$gene))
tumorSignatures <- tumorSignatures[tumorSignatures$gene %in% sharedGenes,]
drugSignatures <- drugSignatures[drugSignatures$gene %in% sharedGenes,]

drugs <- unique(drugSignatures$drug)

# To reproduce the top 8:
drugs <- c("erlotinib","elvitegravir","tenofovir","trimidox",
           "nicotinamide","quinine","genistein","temsirolimus")

sig_ids <- unique(tumorSignatures$signature_id)

o <-
  data.frame(
    drug = rep(NA, each=length(sig_ids)*length(drugs)),
    signature_id = NA,
    connectivityscore = NA,
    pvalue = NA,
    stringsAsFactors = FALSE
  )

i <- 1
for (d in drugs){
  
  ### Creates drug signature ###
  print(paste0("Busy with drug ", d))
  
  drugSignature <- subset(drugSignatures, drug==d)
  
  rownames(drugSignature) <- 
    drugSignature$gene
  
  drugSignature <-
    subset(drugSignature, select=c("estimate"))
  
  colnames(drugSignature)[1] <- "est"
  
  for (s in sig_ids){
    
    print(paste0("Busy with signature ",s))
    
    o[i,c("drug","signature_id")] <-
      c(d,s)
    
    ### Creates tumor signature ###
    tumorSignature <-
      subset(tumorSignatures, signature_id==s)
    
    rownames(tumorSignature) <-
      tumorSignature$gene
    
    tumorSignature$fdr <- 
      p.adjust(tumorSignature$pvalue, method="BH")
    
    tumorSignature <-
      subset(tumorSignature, fdr < 0.01, select=c("est"))
    
    if (nrow(tumorSignature) > 9){
      
      cs <- PharmacoGx:::connectivityScore(
        x=drugSignature,
        y=tumorSignature,
        method="fgsea",
        nperm = 1000, # 1000 for demo - runs a lot quicker than 10,000
        nthread = 1
      )
      
    } else {
      
      cs <- c(NA,NA)
      
    }
    
    o[i,c("connectivityscore","pvalue")] <-
      cs
    
    i <- i + 1
  }
}

# Results for samples (exact results can vary a bit each time because of random nature of permutation tests):
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & startsWith(signature_id, "TCGA"), select=drug)) 

# Results for other signature types:
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & signature_id == "Group", select=drug)) # Group only
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & (signature_id == "1"), select=drug)) # Subtype 1
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & (signature_id == "2"), select=drug)) # Subtype 2
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & (signature_id == "3"), select=drug)) # Subtype 3
table(subset(o, pvalue < 0.05 & connectivityscore < 0 & (signature_id == "4"), select=drug)) # Subtype 4

load("ProcessedData/original_output.RData") # The full output of the original analysis