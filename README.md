# personalised_DR_ccRCC
Code and data accompanying the 'Personalised drug repositioning for Clear Cell Renal Cell Carcinoma using gene expression' paper

The main code is split into 3 parts:
1. createTumorSignatures.R
2. createDrugSignatures.R
3. compareTumorAndDrugSignatures.R

To keep these parts as short and clear as possible, code to do the more detailed stuff is implemented in functions (see 'Functions' folder).

The packages needed to run each part are specified at the top of each script.  

### Addional data and code accompanying follow-up paper:
'The impact of estimated tumour purity on gene expression-based 
drug repositioning of Clear Cell Renal Cell Carcinoma samples ###

All these new files are included in the TumourPuritySensitivityAnalysis subdirectory.
