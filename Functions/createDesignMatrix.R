createDesignMatrix <- function(concentration,type,batch,duration){
  
  ff <- sprintf("~ concentration")
  
  if (length(sort(unique(type))) > 1) {
    ff <- sprintf("%s + type", ff)
  }
  
  if (length(sort(unique(batch))) > 1) {
    ff <- sprintf("%s + batch", ff)
  }
  if (length(sort(unique(duration))) > 2) {
    ff <- sprintf("%s + duration", ff)
  }
  
  design <- 
    model.matrix.default(formula(ff))
  
  return(design)
}