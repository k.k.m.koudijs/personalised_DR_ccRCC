combineTumorSignatures <- function(path){
  
  load(paste0(path,"/DEG_tumorGroup.RData"))
  load(paste0(path,"/DEG_tumorSubtypes.RData"))
  load(paste0(path,"/DEG_tumorSamples.RData"))
  
  colnames(DEG_tumorSamples)[1] <- "signature_id"
  colnames(DEG_subtypes)[1] <-   "signature_id"
  DEG_group <- cbind(data.frame(signature_id="Group"),DEG_group)
  
  output <- rbind(DEG_group,DEG_subtypes,DEG_tumorSamples)
  
  return(output)
  
}