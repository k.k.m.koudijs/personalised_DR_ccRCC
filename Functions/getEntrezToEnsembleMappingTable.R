getEntrezToEnsembleMappingTable <- function(mappingTable,entrez_ids){
  
  mappingTable <-
    mappingTable[mappingTable$entrezgene %in% entrez_ids, ] 
  toMultipleEnsembl <-
    unique(mappingTable[duplicated(mappingTable$entrezgene),"entrezgene"]) 
  mappingTable <-
    mappingTable[!(mappingTable$entrezgene %in% toMultipleEnsembl), ]
  
  duplicatedEnsg <-
    as.character(subset(
      as.data.frame(table(subset(mappingTable, select=c("ensembl_gene_id")))),
      Freq > 1
    )$Var1)
  
  mappingTable <-
    mappingTable[!(mappingTable$ensembl_gene_id %in% duplicatedEnsg), ]
  
  return(mappingTable)
  
}