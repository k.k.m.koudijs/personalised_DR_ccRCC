#### Run this script first, next all figures and tables in order ###
setwd("TumourPuritySensitivityAnalysis")
source("settings.R")
load("Input/multipleTumorAnalyses.RData")

TCGA_Toil_sample_matching <- read_csv("Input/TCGA_Toil_sample_matching.csv")
TCGA_Toil_sample_matching <- subset(TCGA_Toil_sample_matching, in_toil==TRUE)

# Data from Systematic pan-cancer analysis of tumour purity
# Nature Communications volume 6, Article number: 8971 (2015)
# doi:10.1038/ncomms9971
### CPE is the median purity level after normalizing levels from all methods to give them equal means and s.d.’s (75.3±18.9%).  ###
tumor_purity <- read_excel(path="Input/ncomms9971-s2.xlsx", skip=2)
tumor_purity$CPE <-  as.numeric(tumor_purity$CPE)
tumor_purity <- subset(tumor_purity, `Cancer type`=="KIRC" & is.finite(CPE))

load("Input/original_output.RData")
tumor_purity <- subset(tumor_purity, select=c("Sample ID","CPE","IHC"))
tumor_purity$CPE <- tumor_purity$CPE * 100
tumor_purity$IHC <- tumor_purity$IHC * 100
save(tumor_purity, file="Input/KIRC_tumor_purity.RData")

drugs <- c("erlotinib","elvitegravir","tenofovir","trimidox",
           "nicotinamide","quinine","genistein","temsirolimus",
           "CAY-10585","PX-12","YC-1") 

o <- o[o$drug %in% drugs,]
o <- o[startsWith(o$signature_id,"TCGA"),]
o <- o[!is.na(o$connectivityscore),]
o$sample_id <- substr(o$signature_id,1,16)

shared_samples <- intersect(unique(o$sample_id),tumor_purity$`Sample ID`)
shared_samples <- intersect(shared_samples, TCGA_Toil_sample_matching$sample)

kirc_samples <- o[o$sample_id %in% shared_samples,]
kirc_samples <- merge(kirc_samples, tumor_purity, by.x = "sample_id", by.y = "Sample ID")

kirc_sample_ids <- subset(kirc_samples, drug=="erlotinib")$sample_id # 525 sample IDs: 4 samples occur 4 times (521 unique IDs)
duplicated_samples <- kirc_sample_ids[duplicated(kirc_sample_ids)]

# Solution: Average mean connectivity scores of duplicated samples and remove 1:
for (s in duplicated_samples){
  for (d in unique(kirc_samples$drug)){
    
    kirc_samples[kirc_samples$sample_id == s & kirc_samples$drug == d,"connectivityscore"] <-
      mean(kirc_samples[kirc_samples$sample_id == s & kirc_samples$drug == d,"connectivityscore"])
    
    kirc_samples[kirc_samples$sample_id == s & kirc_samples$drug == d,"pvalue"] <-
      mean(kirc_samples[kirc_samples$sample_id == s & kirc_samples$drug == d,"pvalue"])
    
  }
}

unique(kirc_samples[kirc_samples$sample_id %in% duplicated_samples,"signature_id"])
remove_these_duplicates <- 
  c("TCGA-B2-3923-01A-02R-A277-07","TCGA-B2-3924-01A-02R-A277-07","TCGA-B2-5633-01A-01R-A277-07","TCGA-B2-5635-01A-01R-A277-07")

kirc_samples <- kirc_samples[!(kirc_samples$signature_id %in% remove_these_duplicates),]
tumor_purity <- tumor_purity[tumor_purity$`Sample ID` %in% unique(kirc_samples$sample_id),]