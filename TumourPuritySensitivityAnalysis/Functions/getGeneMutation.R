getGeneMutation <- function(gene_id, sample_id){
  
  sample_mutations <- 
    mutation_all[startsWith(mutation_all$Tumor_Sample_Barcode, sample_id),]
  
  if (nrow(sample_mutations) > 0){
    
    sample_mutations <- 
      subset(sample_mutations, Hugo_Symbol == gene_id & (IMPACT=="MODERATE" | IMPACT=="HIGH"))
    
   return(nrow(sample_mutations) > 0)
      
  } else {
    return(NA)
  }
  
}