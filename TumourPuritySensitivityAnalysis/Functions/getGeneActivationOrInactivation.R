getGeneActivationOrInactivation <- function(gene_id, sample_id, chr_expectation, include_mutation=TRUE){
  
  getmode <- function(v) {
    uniqv <- unique(v)
    uniqv[which.max(tabulate(match(v, uniqv)))]
  }
  
  if (chr_expectation != "Activation" & chr_expectation != "Inactivation"){
    
    stop("chr_expectation should be 'Activation' or 'Inactivation'")
    
  } else {
    
    if (include_mutation){
      
      has_mutation <- NA
      sample_mutations <- 
        mutation_all[startsWith(mutation_all$Tumor_Sample_Barcode, sample_id),]
      
      if (nrow(sample_mutations) > 0){
        
        sample_mutations <- 
          subset(sample_mutations, Hugo_Symbol == gene_id & (IMPACT=="MODERATE" | IMPACT=="HIGH"))
          
        has_mutation <- 
          nrow(sample_mutations) > 0
      }
      
    } else {
      
      has_mutation <- FALSE
      
    }
    
    has_chr_alteration <- NA
    sample_index <- which(colnames(gistic.thresholedbygene) == sample_id)
    
    if (length(sample_index) > 0){
      
      sample_alterations <- 
        gistic.thresholedbygene[,c(1:3,sample_index)]
      
      sample_alterations <- 
        subset(sample_alterations, Gene.Symbol==gene_id)
      
      chr_event <- getmode(as.integer(sample_alterations[,4]))
      
      if (chr_expectation == "Inactivation" & chr_event < 0){
        has_chr_alteration <- TRUE
      } else if (chr_expectation == "Activation" & chr_event > 0){
        has_chr_alteration <- TRUE
      } else {
        has_chr_alteration <- FALSE
      }
    }
    
    if (is.na(has_mutation) & is.na(has_chr_alteration)){
      
      return(NA)
      
    } else if (is.na(has_mutation)){
      
      if (has_chr_alteration){
        return(TRUE)
      } else {
        return(NA)
      }
      
    } else if (is.na(has_chr_alteration)){
      
      if (has_mutation){
        return(TRUE)
      } else {
        return(NA)
      }
      
    } else {
      
      return(has_mutation | has_chr_alteration)
      
    } 
  }
}