fig3a_input <- kirc_samples[kirc_samples$drug %in% c("CAY-10585","PX-12","YC-1"),]
fig3a_input$Drug <- factor(fig3a_input$drug, c("CAY-10585","PX-12","YC-1"), ordered = TRUE)

fig3a_input$Connectivity <- "Neutral"
fig3a_input$Connectivity[fig3a_input$connectivityscore > 0] <- "Positive"
fig3a_input$Connectivity[fig3a_input$connectivityscore < 0] <- "Negative"
fig3a_input$Connectivity <- factor(fig3a_input$Connectivity, levels = c("Positive","Neutral","Negative"), ordered = TRUE)



quantile(tumor_purity$CPE, probs=c(0.3333333,0.666667))
fig3a_input$TumorPurity <- "60-72%"
fig3a_input$TumorPurity[fig3a_input$CPE < 60.06] <- "< 60%"
fig3a_input$TumorPurity[fig3a_input$CPE > 72.23000] <- "> 72%"
fig3a_input$TumorPurity <- factor(fig3a_input$TumorPurity, levels = c("< 60%","60-72%","> 72%"), ordered = TRUE)

fig3a <- ggplot(data=fig3a_input, aes(x=CPE, y=connectivityscore, color=Drug)) + geom_smooth() + labs(
    x = "Estimated tumour purity",
    y = "Connectivity score"
  ) + scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")) + 
  theme(text = element_text(size=24)) + scale_colour_manual(values=c("orange","purple","cyan2"))

fig3b_input <- as.data.frame(table(fig3a_input[,c("Drug","Connectivity","TumorPurity")]))
fig3b_input <- fig3b_input[fig3b_input$Drug %in% c("CAY-10585","PX-12"),]
fig3b_input$Connectivity <- factor(fig3b_input$Connectivity, levels = c("Positive","Neutral","Negative"), ordered = TRUE)

fig3b <- ggplot(data=fig3b_input) + 
  aes(
    x=TumorPurity, 
    y=Freq,
    fill=Connectivity
  ) + geom_bar(stat = 'identity', position = 'stack') + labs(
    x = "Estimated tumour purity",
    y = "Count"
  ) + facet_grid(~ Drug) + theme(text = element_text(size=24)) + scale_fill_manual(values=c("#00BA38","#619CFF","#F8766D"))   

wilcox.test(subset(fig3a_input, Drug=="CAY-10585" & TumorPurity=="< 60%")$connectivityscore, subset(fig3a_input, Drug=="CAY-10585" & TumorPurity=="60-72%")$connectivityscore)
wilcox.test(subset(fig3a_input, Drug=="PX-12" & TumorPurity=="< 60%")$connectivityscore, subset(fig3a_input, Drug=="PX-12" & TumorPurity=="60-72%")$connectivityscore)

wilcox.test(subset(fig3a_input, Drug=="CAY-10585" & TumorPurity=="60-72%")$connectivityscore, subset(fig3a_input, Drug=="CAY-10585" & TumorPurity=="> 72%")$connectivityscore)
wilcox.test(subset(fig3a_input, Drug=="PX-12" & TumorPurity=="60-72%")$connectivityscore, subset(fig3a_input, Drug=="PX-12" & TumorPurity=="> 72%")$connectivityscore)

fig3c_input <- merge(
  x = subset(fig3a_input, drug=="CAY-10585"), 
  y = subset(fig3a_input, drug=="PX-12"), 
  by="signature_id"
)
fig3c_input$`Tumour purity` <- factor(fig3c_input$TumorPurity.x, c("< 60%","60-72%","> 72%"), ordered = TRUE)

fig3c <- ggplot(data=fig3c_input, aes(x=connectivityscore.x, y=connectivityscore.y, color=`Tumour purity`)) + geom_point() + labs(
  x = "Connectivity score of CAY-10585",
  y = "Connectivity score of PX-12"
) + scale_colour_manual(values=c("orange","cyan2","purple")) + theme(text = element_text(size=24))

cor.test(fig3c_input$connectivityscore.x,fig3c_input$connectivityscore.y, method = "spearman") # Rho 0.56

fig3d_input <- fig3c_input
fig3d_input$`HIF activation` <- "Less likely (N=214)"
fig3d_input$`HIF activation`[fig3d_input$connectivityscore.x < 0 & fig3d_input$connectivityscore.y < 0] <- "More likely (N=307)"

fig3d_input$`HIF activation` <- 
  factor(fig3d_input$`HIF activation`, levels = c("More likely (N=307)","Less likely (N=214)"), ordered = TRUE)

fig3d <- ggplot(data=fig3d_input, aes(x=CPE.x, color=`HIF activation`)) + geom_density() + labs(
  x = "Estimated tumour purity",
  y = "Density"
) + scale_colour_manual(values=c("#F8766D","#00BFC4")) + 
  scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")) + 
  theme(text = element_text(size=24))

wilcox.test(
  subset(fig3d_input, `HIF activation`=="More likely (N=307)")$CPE.x, 
  subset(fig3d_input, `HIF activation`=="Less likely (N=214)")$CPE.x
)

ggarrange(fig3a,fig3b, fig3c, fig3d, ncol = 2)
