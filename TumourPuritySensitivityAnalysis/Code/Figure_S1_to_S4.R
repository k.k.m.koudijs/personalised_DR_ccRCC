# Figures with gene level exlusion analysis

library("PharmacoGx")
options(stringsAsFactors = FALSE)
load(file="../ProcessedData/drugSignatures.Rdata")
drugs <- c("erlotinib","elvitegravir","tenofovir","trimidox","nicotinamide","quinine","genistein","temsirolimus")
drugSignatures <- drugSignatures[drugSignatures$drug %in% drugs & drugSignatures$gene %in% unique(DEG_tumorSamples$gene),]

# Goal: regress genes on gene level, and
# Exclude upregulated genes which are negatively associated with increased tumor purity (i.e. become less upregulated at higher purity)
# Exclude downregulated genes which are positively associated with increased tumor purity (i.e. become less downregulated at higher purity)

load("Input/DEG_tumorSamples.RData")
DEG_tumorSamples$gene <- as.character(DEG_tumorSamples$gene)

sharedGenes <- intersect(DEG_tumorSamples$gene, drugSignatures$gene)
length(unique(DEG_tumorSamples$gene)) # Contains all 11,333 genes

DEG_tumorSamples <- DEG_tumorSamples[DEG_tumorSamples$gene %in% sharedGenes,]
drugSignatures <- drugSignatures[drugSignatures$gene %in% sharedGenes,]
length(unique(DEG_tumorSamples$gene)) # 495 genes left

# Run Table_1_2 script and all previous scripts first
length(unique(DEG_tumorSamples$sample)) # Contains all 538 samples
DEG_tumorSamples <- DEG_tumorSamples[DEG_tumorSamples$sample %in% regression_input$signature_id,]
length(unique(DEG_tumorSamples$sample)) # Contains included 521 samples

# Combine with tumor purity and other variables
DEG_tumorSamples <- merge(x=DEG_tumorSamples, y=regression_input, by.x="sample",by.y="signature_id")

# Calculate FDR:
samples <- unique(DEG_tumorSamples$sample)
DEG_tumorSamples$fdr <- NA
for (i in 1:length(samples)){
  
  print(i)
  DEG_tumorSamples$fdr[DEG_tumorSamples$sample == samples[i]] <-
    p.adjust(DEG_tumorSamples$pvalue[DEG_tumorSamples$sample == samples[i]], method="BH")
  
}

# Now test association for each gene
gene_level_purity_association <- data.frame(
  Gene = unique(DEG_tumorSamples$gene),
  PctUpregulation = NA,
  Rho = NA,
  pvalue = NA, 
  stringsAsFactors = FALSE
)

for (i in 1:nrow(gene_level_purity_association)){
  
  g <- gene_level_purity_association[i,"Gene"]
  df <- subset(DEG_tumorSamples, gene==g)
  test_result <- cor.test(x=df$est, y=df$CPE, method="spearman")
  
  gene_level_purity_association[i,"PctUpregulation"] <- 100 * sum(df$est > 0) / nrow(df)
  gene_level_purity_association[i,"Rho"] <- as.numeric(test_result$estimate)
  gene_level_purity_association[i,"pvalue_rho"] <- test_result$p.value

}

gene_level_purity_association$fdr_rho <- 
  p.adjust(gene_level_purity_association$pvalue_rho, method="BH")

sum(gene_level_purity_association$fdr < 0.01) / nrow(gene_level_purity_association) # 52% with FDR < 1% !!!
sum(gene_level_purity_association$fdr < 0.1) / nrow(gene_level_purity_association) # 65% with FDR < 10% !!!
# Proof 1: Many genes have an association with estimated tumor purity!

# Create table with:
# - First row: typically (>50%) upregulated) genes: 
# - First row, first column: logFC increases with increasing tumor purity (good)
# - First row, second column: logFC does not increase or decrease with increasing tumor purity (neutral)
# - First row, third column: logFC decreases with increasing tumor purity (bad)
# - Second row: typically (>50%) downregulated) genes: 
# - Second row, first column: logFC increases with increasing tumor purity (bad)
# - Second row, second column: logFC does not increase or decrease with increasing tumor purity (neutral)
# - Second row, third column: logFC decreases with increasing tumor purity (good)

gene_level_purity_association$TypicalDirection <- ifelse(
  gene_level_purity_association$PctUpregulation > 50, "Typically (>50%) upregulated", "Typically (>50%) downregulated"
)
gene_level_purity_association$TypicalDirection <- factor(
  gene_level_purity_association$TypicalDirection, 
  levels = c("Typically (>50%) upregulated", "Typically (>50%) downregulated"),
  ordered = TRUE
)

gene_level_purity_association$AssociationWithTumorPurity <- "Neutral"
gene_level_purity_association$AssociationWithTumorPurity[
  gene_level_purity_association$pvalue_rho < 0.05 & gene_level_purity_association$Rho > 0] <- "More positive at higher purity"

gene_level_purity_association$AssociationWithTumorPurity[
  gene_level_purity_association$pvalue_rho < 0.05 & gene_level_purity_association$Rho < 0] <- "More negative at higher purity"

gene_level_purity_association$AssociationWithTumorPurity <- factor(
  gene_level_purity_association$AssociationWithTumorPurity, 
  levels = c("More negative at higher purity", "Neutral","More positive at higher purity"), 
  ordered = TRUE
)



gene_level_purity_association_summary <- as.data.frame(table(gene_level_purity_association[,c("TypicalDirection","AssociationWithTumorPurity")]))
# Proof 2: It typically goes in the opposite direction that it is expected of (i.e. upregulated more positive, downregulated less so).


groups <- c("More differentially expressed with increasing tumour purity", "Insensitive to increasing tumour purity", "Less differentially expressed with increasing tumour purity")
Figure_S1_input <- data.frame(
  Direction = factor(
    x=groups, levels=groups, ordered = TRUE
  )
)
Figure_S1_input$`Number of genes` <- NA
Figure_S1_input[1,"Number of genes"] <- 
    sum(
      c(
        subset(gene_level_purity_association_summary, TypicalDirection=="Typically (>50%) upregulated" & AssociationWithTumorPurity=="More positive at higher purity")$Freq,
        subset(gene_level_purity_association_summary, TypicalDirection=="Typically (>50%) downregulated" & AssociationWithTumorPurity=="More negative at higher purity")$Freq
      )
    )
Figure_S1_input[2,"Number of genes"] <- sum(subset(gene_level_purity_association_summary, AssociationWithTumorPurity=="Neutral")$Freq)
Figure_S1_input[3,"Number of genes"] <- sum(
  c(
    subset(gene_level_purity_association_summary, TypicalDirection=="Typically (>50%) upregulated" & AssociationWithTumorPurity=="More negative at higher purity")$Freq,
    subset(gene_level_purity_association_summary, TypicalDirection=="Typically (>50%) downregulated" & AssociationWithTumorPurity=="More positive at higher purity")$Freq
  )
)

Figure_S1 <- ggplot(data=Figure_S1_input, aes(x=Direction, y=`Number of genes`, fill=Direction)) + 
  geom_bar(stat="identity") + theme(
    text = element_text(size=24), legend.position = "none"
  ) + scale_fill_manual(values=c("#00BA38","#619CFF","#F8766D")) + xlab("Association with estimated tumour purity")  
print(Figure_S1)


# Proof 3: show impact of excluding genes on tumor sample signature size
DEG_tumorSamples <- merge(x=DEG_tumorSamples, y=gene_level_purity_association, by.x="gene", by.y="Gene")
DEG_tumorSamples <- subset(DEG_tumorSamples, fdr < 0.01) # same cut-off as original analysis

DEG_tumorSamples$exclude_gene <- 
  (
    DEG_tumorSamples$est > 0 & # Gene is upregulated in sample...
    DEG_tumorSamples$Rho < 0 & DEG_tumorSamples$pvalue_rho < 0.05# But overall, gene becomes more negative at higher purity
   
  ) | (
    DEG_tumorSamples$est < 0 & # Gene is downregulated in sample...
    DEG_tumorSamples$Rho > 0 & DEG_tumorSamples$pvalue_rho < 0.05 # But overall, gene becomes more positive at higher purity
  )
View(DEG_tumorSamples[,c("est","Rho","pvalue_rho","exclude_gene")])


# Check
names(table(subset(DEG_tumorSamples, exclude_gene==FALSE)[,c("sample")]))==names(table(DEG_tumorSamples[,c("sample")]))

Figure_S2_input <- data.frame(
  Sample = names(table(subset(DEG_tumorSamples, exclude_gene==FALSE)[,c("sample")])),
  Condition = c(
    rep("Before exclusion", times=length(samples)), 
    rep("After exclusion", times=length(samples))
  ),
  SignatureSize = c(
    as.integer(table(DEG_tumorSamples[,c("sample")])), # Signature size before exclusion
    as.integer(table(subset(DEG_tumorSamples, exclude_gene==FALSE)[,c("sample")])) # Signature sizes after exclusion
  )
)
Figure_S2_input$Condition <- factor(
  x = Figure_S2_input$Condition, levels = c("Before exclusion","After exclusion"), ordered = TRUE
)


Figure_S2 <- ggplot(data=Figure_S2_input, aes(x=SignatureSize, color=Condition)) + 
  geom_density() + scale_color_manual(values=c("#00BA38","#F8766D")) + labs(
    x = "Tumour sample signature size used to calculate connectivity score",
    y = "Density"
  ) + 
  scale_fill_manual(values=c("#00BA38","#F8766D")) + theme(
    text = element_text(size=30)
  ) 
print(Figure_S2)

# Now calculate connectivity scores for top 8 drugs using 1) All genes before exclusion and 2) Subset of genes after exclusion
# Note: Only possible for signatures which have at least 10 genes in both signatures.


o <-
  data.frame(
    drug = rep(NA, each=2*length(samples)*length(drugs)),
    sample = NA,
    condition = NA,
    connectivityscore = NA,
    pvalue = NA,
    stringsAsFactors = FALSE
  )

i <- 1
conditions <- c("Before exclusion","After exclusion")
for (d in drugs){
  
  ### Creates drug signature ###
  print(paste0("Busy with drug ", d))
  
  drugSignature <- subset(drugSignatures, drug==d)
  
  rownames(drugSignature) <- 
    drugSignature$gene
  
  drugSignature <-
    subset(drugSignature, select=c("estimate"))
  
  colnames(drugSignature)[1] <- "est"
  
  for (s in samples){
    
    print(paste0("Busy with sample ",s))
    
    ### Creates tumor signature ###
    tumorSignature <-
      subset(DEG_tumorSamples, sample==s)
    
    rownames(tumorSignature) <-
      tumorSignature$gene
    
    for (condition in conditions){
      
      print(paste0("Busy with condition ",condition))
      o[i,c("drug","sample","condition")] <- c(d,s,condition)
            
      if (condition == "Before exclusion"){
        tumorSignatureSubset <- tumorSignature
      } else if (condition == "After exclusion"){
        tumorSignatureSubset <- subset(tumorSignature, exclude_gene==FALSE)
      } 
      
      tumorSignatureSubset <- tumorSignatureSubset[,"est",drop=FALSE]
      
      if (nrow(tumorSignatureSubset) > 9){
        
        cs <- PharmacoGx:::connectivityScore(
          x=drugSignature,
          y=tumorSignatureSubset,
          method="fgsea",
          nperm = 10000, 
          nthread = 1
        )
        
      } else {
        
        cs <- c(NA,NA)
        
      }
      
      o[i,c("connectivityscore","pvalue")] <-
        cs
      
      i <- i + 1
      
    }
  }
}

samples_with_2_connectivity_scores <- subset(as.data.frame(table(subset(o, !is.na(connectivityscore))$sample)), Freq==16)$Var1
o <- o[o$sample %in% samples_with_2_connectivity_scores,] # 500 samples left

Figure_S3_input <- o
colnames(Figure_S3_input) <- c("Drug","Sample","Condition","Connectivity score","pvalue")
Figure_S3_input$Drug <- factor(Figure_S3_input$Drug, levels=drugs, ordered = TRUE)
Figure_S3_input$Condition <- factor(Figure_S3_input$Condition, levels=conditions, ordered = TRUE)
save(Figure_S3_input, file="Output/Figure_S3_input.RData")

Figure_S3 <- ggplot(data=Figure_S3_input, aes(x=Drug, y=`Connectivity score`, color=Condition)) + 
  geom_boxplot() + scale_color_manual(values=c("#00BA38","#F8766D")) + theme(
    text = element_text(size=30)
  ) 

for (d in drugs){
  print(d)
  print(wilcox.test(
    x = subset(Figure_S3_input, Drug==d & Condition=="Before exclusion")$`Connectivity score`,
    y = subset(Figure_S3_input, Drug==d & Condition=="After exclusion")$`Connectivity score`
  ))
}
print(Figure_S3)


Figure_S4_chisquare_input  <-
  as.data.frame(table(subset(Figure_S3_input, pvalue < 0.05 & `Connectivity score` < 0)[,c("Drug","Condition")]))

for (d in drugs){
  print(d)
  
  M <- as.table(rbind(c(
    subset(Figure_S4_chisquare_input, Drug==d & Condition=="Before exclusion")$Freq, 
    length(samples_with_2_connectivity_scores) - subset(Figure_S4_chisquare_input, Drug==d & Condition=="Before exclusion")$Freq
    ), 
    c(
      subset(Figure_S4_chisquare_input, Drug==d & Condition=="After exclusion")$Freq,
      length(samples_with_2_connectivity_scores) - subset(Figure_S4_chisquare_input, Drug==d & Condition=="After exclusion")$Freq
    )))
  dimnames(M) <- list(Condition = c("Before", "After"),
                      NegativelyEnriched = c("Yes","No"))
  
  print(chisq.test(
    x = M
  ))
  
}

Figure_S4_input  <-
  as.data.frame(table(subset(Figure_S3_input, pvalue < 0.05 & `Connectivity score` < 0)[,c("Drug","Condition")]) * 100 / length(samples_with_2_connectivity_scores))

Figure_S4 <- ggplot(data=Figure_S4_input, aes(x=Condition, y=Freq, fill=Condition)) + labs(
  x = "Drug",
  y = "Negative enrichment (% with P < 0.05)"
) + geom_bar(stat = 'identity', position = 'stack') + facet_grid(~ Drug) + 
  scale_fill_manual(values=c("#00BA38","#F8766D")) + theme(
    axis.text.x = element_blank(),
    text = element_text(size=30)
  ) 

print(Figure_S4)
