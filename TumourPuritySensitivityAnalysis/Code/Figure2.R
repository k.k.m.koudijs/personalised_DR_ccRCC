ggarrange(
  ggplot(subset(kirc_samples, drug=="erlotinib"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Erlotinib",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="elvitegravir"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Elvitegravir",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="tenofovir"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Tenofovir",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="trimidox"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Trimidox",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="nicotinamide"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Nicotinamide",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="quinine"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Quinine",
    y = "Connectivity score",
    x = "Estimated tumour purity"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="genistein"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Genistein",
    x = "Estimated tumour purity",
    y = "Connectivity score"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ggplot(subset(kirc_samples, drug=="temsirolimus"), aes(x=CPE, y=connectivityscore)) + geom_point() + labs(
    title = "Temsirolimus",
    x = "Estimated tumour purity",
    y = "Connectivity score"
  ) + stat_smooth() + theme(text = element_text(size=30)) + 
    scale_x_continuous(breaks=c(20,40,60,80,100), labels=c("20%","40%","60%","80%","100%")),
  ncol = 2)


for (d in drugs){
  
  data <- subset(kirc_samples, drug==d)
  
  print(d)
  test_result <- cor.test(data$CPE, data$connectivityscore, method="pearson")
  print(paste0("Rho = ",as.numeric(test_result$estimate),", P =",test_result$p.value))
  
  test_result <- wilcox.test(
    x = subset(data, CPE < 80)$connectivityscore, 
    y = subset(data, CPE > 80)$connectivityscore
  )
  print(paste0("P value <80% versus >80% = ",test_result$p.value))
  
}
